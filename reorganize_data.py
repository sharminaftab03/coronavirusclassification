import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
import csv, os


def reorganized_data():
    data = pd.read_csv('/Users/d441874/Documents/medium/2-CoronaXrayChect/coronahack-chest-xraydataset/Chest_xray_Corona_Metadata.csv')
    csv_rows_train = []
    csv_rows_test = []
    for i in range(len(data)):
        image_name = data.iloc[i].X_ray_image_name
        disease = data.iloc[i].Label_2_Virus_category
        dataset_type = data.iloc[i].Dataset_type
        virus_category = data.iloc[i].Label_1_Virus_category
        number = data.iloc[i].Number
        if pd.isna(virus_category):
            class_label = "normal"
        elif virus_category == "bacteria":
            if pd.isna(disease):
                class_label = "bacterial-pnemonia"
            else:
                class_label = disease
        elif virus_category == "Virus":
            if pd.isna(disease):
                class_label = "viral-pnemonia"
            else:
                class_label = disease
        else:
            class_label = disease
        if dataset_type == "TRAIN":
            csv_rows_train.append([number,image_name,class_label,dataset_type])
        else:
            csv_rows_test.append([number,image_name," ",dataset_type])
    return csv_rows_train,csv_rows_test


def create_csv(filename,train=True):
    csv_rows_train, csv_rows_test = reorganized_data()
    column_names = ['Number','X_ray_image_name','XRay-Result','Dataset_type']
    with open(filename, 'w') as csvfile:
        filewriter = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(column_names)
        if train:
            for row in csv_rows_train:
                filewriter.writerow(row)
        else:
            for row in csv_rows_test:
                filewriter.writerow(row)
 
create_csv("dataset/train.csv")
create_csv("dataset/test.csv",False)
